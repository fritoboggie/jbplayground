package frito.jbp.bl;

/**
 * Class abstract
 * <p/>
 * Class description
 * <p/>
 *
 * @author frito
 */
public interface EchoerIF {

    String echo(String text);

}

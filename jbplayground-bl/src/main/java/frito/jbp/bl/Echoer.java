package frito.jbp.bl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class abstract
 * <p/>
 * Class description
 * <p/>
 *
 * @author frito
 */
public class Echoer implements EchoerIF {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(Echoer.class);

    @Override
    public String echo(String text) {
        LOG.info("echo: echoing! text={}", text);
        return text;
    }
}

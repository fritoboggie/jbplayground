package frito.jbp.ejb;

import frito.jbp.bl.Echoer;
import frito.jbp.bl.EchoerIF;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Class abstract
 * <p/>
 * Class description
 * <p/>
 *
 * @author frito
 */
@Stateless
@Remote(EchoerIF.class)
public class EchoerBean implements EchoerIF {

    private EchoerIF delegate = new Echoer();

    @Override
    public String echo(String text) {
        return delegate.echo(text);
    }
}

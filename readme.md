# JBoss Playground (MASTER)

This is a small gradle project with simple JBoss management capabilities. The project is designed to create and share more or less
simple reproducibles, examples or prototypes using JBoss EAP 7.x .

The project is stored within a GIT repository. The starting point is a simple remote EJB access on an application, deployed as EAR
(master branch). Please create a new branch for new code examples. Feel free to add, move or delete subprojects as needed for your example.

Don't forget to add branch specific documentation to this `readme.md` file as needed.

## Content

* `jbplayground`: root project, some basic configurations
* `jbplayground-bl`: business logic
* `jbplayground-ejb`: EJBs
* `jbplayground-ear`: EAR
* `jbplayground-jboss-test`: subproject containing the JBoss management capabilities, a CLI configuration file and the JUnit enteprise test

## Usage

### Preconditions

The project on MASTER comes with a Gradle wrapper. Executable tasks are designed for Mac or Linux environment. You need JDK 8 and git installed.
You have to provide a clean (unchanged) JBoss EAP 7.1.5 installation. The runtime JBoss instance will be copied from there. This installation
will not be changed.

### Used JBoss Version / JBoss configuration

The MASTER branch is configured to use JBoss EAP 7.1.5. If an example / branch needs another verion,
read the 
Customization is done based on `standalone.xml`.

If a branch of this project needs a differen JBoss version, it is documented within the branch specific chapter.

### Configuration

You need to provide a gradle property `jbossTemplateDir` pointing to your vanilla JBoss installation. This can be done by passing it when calling
a build task e.g. by using
```
./gradlew -PjbossTemplateDir=/jboss/eap-7.1.5/jboss-eap-7.1 <task>
```
or by providing a `gradle.properties` file within the projects root directory containing this property.

The customization (prepareing the Jboss runtime) of your JBoss instance is done by executing the CLI file `jbplayground-jboss-test/src/main/resources/cli/jboss-config.cli`.

### Change JBoss Version / JBoss configuration

#### JBoss version

Specify the location of the template Jboss and the used module dependencies:

* Change the property `jbossTemplateDir` to point to a matching JBoss version
* Change the BOM versions used in the `dependencyManagement` block within the root projects build file

When moving to another JBoss major or minor version, you have to specify the directory name of the JBoss
root directory:

* Change the value of `jbossDir` within the build file of the `jbplayground-jboss-test` subproject. 
 
#### JBoss Configuration

Change the value of `jbossConfig` within the build file of the `jbplayground-jboss-test` subproject.

### Fully Automatic Test with Gradle

You can call `./gradlew enterpriseTest` in the root directory. Take a look at the test report `jbplayground-jboss-test/build/reports/tests/test/index.html` 
afterwards.

### Using your IDE

Follow these steps to use your IDE for a better development / prototyping / test experience:

1. Import the Gradle projects to your IDE with JBoss management support. 
1. Call `./gradlew jbossCustomizeOnly`. The now configured JBoss instance is located in `jbplayground-jboss-test/build/jboss-eap-7.1`.
Configure this directory as root directory of your managed JBoss instance.
1. Configure your IDE to call the gradle task `:jbplayground-ear:assemble` before deploying the EAR `jbplayground-ear/build/libs/jbplayground.ear` or
configure the IDE managed EAR artifact for deployment.
1. Configure your IDE to start your JBoss instance by providing the configuration file `standalone-full.xml` using the `-c` argument.
1. Start / Stop the JBoss instance as needed
1. Run your tests, usually located in the `jbplayground-jboss-test` submodule from your IDE.

### Using some single Gradle Tasks

If your IDE does not support JBoss management or for other reasons, you might want to call some of the tasks manually. Here is a short explanation of some
JBoss management tasks:

* `jbossDelete`: Removes a JBoss runtime environment. Call this when JBoss is not running.
* `jbossCopy` : Copies the JBoss template files to the runtime environment, if not present. Call this if JBoss is not running.
* `jbossStart`: Starts the JBoss instance. Call this, if JBoss is not running.
* `jbossStop`: Stops the JBoss instance. Call this, if JBoss is running.
* `jbossPrepare`: Executes the CLI script `jbplayground-jboss-test/src/main/resources/cli/jboss-config.cli`. Call this if JBoss is running.
* `jbossReset`: Does a reset on the actual jboss configuration by copying the template configuration. Call this if JBoss is not running.
* `jbossCustomize`: Will do a reset and prepare. Call this, if JBoss is not running. JBoss is still running afterward. Use this
if you want to configure and start JBoss once, before changing code, deploying and undeploying several times afterwards. 
* `jbossCustomizeOnly`: Will do a reset and prepare. Call this, if JBoss is not running. JBoss is not running afterwards. Use
this if you want to change the JBoss configuration and work with your IDE afterwards.
* `jbossDeploy`: Will deploy the EAR from `jbplayground-ear`. Call this if JBoss is running and the EAR is not deployed yet.
* `jbossUndeploy`: Will undeploy the EAR from `jbplayground-ear`. Call this if JBoss is running and the EAR is deployed.

## Possible improvements for the master branch / basic starting point

* replacing hard coded version numbers and file names to simply switch JBoss version, configurations, ...
* add H2 support
* use dockerized JBoss / DB
* use our `omnidroid` plugin for a better JBoss management (needs removing company specific dependencies and open sourcing the plugin)
* ...

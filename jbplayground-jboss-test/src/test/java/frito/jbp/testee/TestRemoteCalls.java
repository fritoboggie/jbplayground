package frito.jbp.testee;

import frito.jbp.bl.EchoerIF;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;

import static org.assertj.core.api.Assertions.*;

/**
 * Class abstract
 * <p/>
 * Class description
 * <p/>
 *
 * @author frito
 */
public class TestRemoteCalls {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(TestRemoteCalls.class);

    private Context jndiContext;

    @Before
    public void initTest() throws Exception {
        jndiContext = new InitialContext();
    }

    @After
    public void finalizeTest() throws Exception {
        jndiContext.close();
    }

    @Test
    public void testSimpleRemoteEjbCall() throws Exception {
        EchoerIF echoer = lookupEjb();
        String text = "bla";
        assertThat(echoer.echo(text)).isEqualTo(text);
    }

    private EchoerIF lookupEjb() throws Exception {
        EchoerIF echoer = (EchoerIF) jndiContext.lookup("ejb:jbplayground/jbplayground-ejb/EchoerBean!frito.jbp.bl.EchoerIF");
        LOG.info("lookupEjb: looked up ejb. echoer={}", echoer);
        return echoer;
    }

}
